package com.acepabdurohman.belajarsetupgitlab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BelajarSetupGitlabApplication {

	public static void main(String[] args) {
		SpringApplication.run(BelajarSetupGitlabApplication.class, args);
	}
}
